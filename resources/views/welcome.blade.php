<!doctype html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Veripal</title>

        
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/all.css">
        <link rel="icon"  type="image/png" href="images/favicon.ico">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        
        
        
    </head>
    <body>
        <div class="container-fullwidth" id="app">
            @if($searchValue === "")
            <div class="content">
                <div class="flex-center full-height">
                    <div class="col-xs-3 width-input">
                        <div class="flex-center">{{HTML::image('images/logo-asliri.PNG')}}</div><br/><p>
                            <form id="postQuery" method="GET">
                                <div class="form-group flex-center">
                                    <input id="q" name="q" title="Telusuri" aria-label="Cari" size={{ $searchValue === "" ? "60" : "40" }}>
                                    <input id="start" name="start" value="{{$startValue}}" hidden>
                                </div>
                                <div class="form-group flex-center">
                                    <select name='domain' style="width:150px; height:26px;">
                                    <option value=""></option>
                                    <option value="penghuni">Penghuni</option>
                                    <option value="merdeka_news">Merdeka News</option>
                                    <option value="detik_news">Detik news</option>
                                    <option value="badilum_perkara">Badilum Perkara</option>
                                    <option value="makamah_agung_perkara">Makamah Agung Perkara</option>
                                    <option value="makamah_agung_perkara">Makamah Agung Perkara</option>
                                    <option value="idi_online">IDI online</option>
                                    <option value="konsula_klinik">Konsula Klinik</option>
                                    <option value="fps_person">FPS person</option>
                                    <option value="siap_sekolah_sekolah">Siap Sekolah</option>
                                    <option value="anggota_dpr_profiles">Anggota DPR</option>
                                    <option value="anggota_mpr_profiles">Anggota MPR</option>
                                    </select>
                                    &nbsp&nbsp
                                    
                                    <button type="submit" class="btn btn-primary ">Search</button>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row padding-top-10 margin-m-15">
                <div class="col-md-1">
                    <div class="logo-search">{{HTML::image('images/logo-asliri.PNG')}}</div>
                </div>
                <div class="col-md-7">
                    <form id="postQuery" method="GET">
                        <input id="q" name="q" title="Telusuri" aria-label="Cari" size="30">
                        <input id="start" name="start" value="{{$startValue}}" hidden>
                        <select class="form-group" name='domain' style="width:150px; height:26px;">
                            <label for="sel1">Select Domain:</label>
                            <option value=""></option>
                            <option value="penghuni">Penghuni</option>
                            <option value="merdeka_news">Merdeka News</option>
                            <option value="detik_news">Detik news</option>
                            <option value="badilum_perkara">Badilum Perkara</option>
                            <option value="makamah_agung_perkara">Makamah Agung Perkara</option>
                            <option value="idi_online">IDI online</option>
                            <option value="konsula_klinik">Konsula Klinik</option>
                            <option value="fps_person">FPS person</option>
                            <option value="siap_sekolah_sekolah">Siap Sekolah</option>
                            <option value="anggota_dpr_profiles">Anggota DPR</option>
                            <option value="anggota_mpr_profiles">Anggota MPR</option>
                        </select>
                        <button type="submit" class="btn btn-primary submit-button">Search</button>
                    </form>  
                </div>
            </div>    
            @endif  
            <div class="row">
                <div class="col-md-7 offset-md-1">
                @isset($response['took'])
                    <small><b>took : {{ $response['took']  }} ms &nbsp&nbsp total : {{ $response['hits']['total']}} record<b></small>
                @endisset
                </div>
            </div> 
            <p>
            <div class="row">
                <div class="col-md-7 offset-md-1">
                @isset($response['took'])
                    @foreach($response['hits']['hits'] as $record)
                    <div class="row">
                    <div class="col-md-6">
                    {{str_replace('_', ' ', $record['_source']['table_name'])}}
                    </div>
                    <div class="col-md-6 text-right">
                    {{$record['_source']['id']}}
                    </div>
                    </div>
                     
                    <a class="tog" href="#{{$record['_source']['id']}}" data-toggle="collapse"><i class="fas fa-arrow-right"></i></a><br/>
                    <div class="collapse" id="{{$record['_source']['id']}}">
                        <pre><code>
                        {{ json_encode($record['_source'], JSON_PRETTY_PRINT) }}
                        </code>
                        </pre>
                    </div>
                    @endforeach  
                    
                    <br/>
                    <nav aria-label="example">
                        <ul class="pagination">
                            <li class="page-item {{ $startValue === 0 ? 'disabled' : ''}}">
                            <a class="page-link" id="prev" href="#">Previous</a>
                            </li>
                            <?php 
                            $page = $startValue+1;
                            $jumlah_page = ceil($totalPages / 10);
                            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
                            $start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
                            $end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
                            
                            for($i = $start_number; $i <= $end_number; $i++){
                                if ($startValue == 0){
                                    $checkvalue = 1;
                                }else{
                                    $checkvalue = $startValue+1;
                                }
                                
                            $link_active = ($checkvalue == $i)? 'class="page-item active"' : 'class="page-item"';
                            ?>
                            
                            <li <?php echo $link_active; ?> ><a class="page-link pages" href="#"><?php echo $i; ?></a></li>
                            <?php
                            }
        
                            ?>
                            <a class="page-link" id="next" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                @endisset
                </div>
            </div>
        </div>
    </body>
</html>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$("pre code").each(function(){
    var html = $(this).html();
    var pattern = html.match(/\s*\n[\t\s]*/);
    $(this).html(html.replace(new RegExp(pattern, "g"),'\n'));
});
$('#q').keypress(function (e) {
    var code = e.keyCode ? e.keyCode : e.which;
    if (code == 13) {
        $('input[name="start"]').val(0)
        $('form#postQuery').submit();
    }
});
if('{{$searchValue}}' != ''){
    $('#q').val('{{$searchValue}}')
}
$('.tog').click(function(){
    
    $(this).find("i").toggleClass('fa-arrow-down fa-arrow-right');
})
$('#next').on('click', function(e){
    e.preventDefault();
    var startValue = $('#start').val()
    $('input[name="start"]').val(parseInt(startValue)+1)
    $('form#postQuery').submit();
})
$('#prev').on('click', function(e){
    e.preventDefault();
    var startValue = $('#start').val()
    $('input[name="start"]').val(parseInt(startValue)-1)
    $('form#postQuery').submit();
})
$('.pages').on('click', function(e){
    e.preventDefault();
    var startValue = $(this).html()
    console.log(startValue)
    $('input[name="start"]').val(parseInt(startValue)-1)
    $('form#postQuery').submit();
})
var get = [];
location.search.replace('?', '').split('&').forEach(function (val) {
    split = val.split("=", 2);
    get[split[0]] = split[1];
});
if(get['domain'] != ''){
    $("select[name='domain'] option[value="+get['domain']+"]").attr("selected", "selected");
}
</script>