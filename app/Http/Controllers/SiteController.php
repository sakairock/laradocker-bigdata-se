<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Cviebrock\LaravelElasticsearch\Factory;
use Cviebrock\LaravelElasticsearch\Manager;
use Elasticsearch;
use Elasticsearch\Client as ES;
use Elasticsearch\ClientBuilder;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        $response = [];
        $start= 0;
        $json = '';
        $hits = 0;
        $searchValue = '';
        $startValue = 0;
        $sizevalue = 10;
        if(!$request->input('start')){
            $startValue = 0;
            $sizevalue = 10;
        }else{
            $startValue= 10*$request->input('start');
            $start = $request->input('start');
        }

        if ($request->isMethod('get') && $request->input('q') != '') {
            $searchValue = $request->input('q');
            if($request->input('domain') != ''){
                if(strpos($searchValue , ",")){
                    $unfixString = str_replace(', ', ',', $searchValue);
                    $manyFieldArray = explode(',',$unfixString);
                    $json = '{';
                    $json .= '"query" : {';
                    $json .= '"bool": {';
                    $json .= '"must": [';
                    $json .= '{ "match": { "table_name" : "'.$request->input('domain').'" }},';
                    for ($x = 0; $x < count($manyFieldArray); $x++) {
                        $singleFieldArray[$x] = explode(' ',$manyFieldArray[$x]);
                        $newArr = [];
                        if (count($singleFieldArray[$x]) >1 ){
                            for ($i = 0; $i < count($singleFieldArray[$x]); $i++) {
                                if($i != 0){
                                    array_push($newArr , $singleFieldArray[$x][$i]);
                                }
                            }
                            $finalQuery = implode(' ', $newArr);
                            
                            if(count($manyFieldArray)-1 == $x){
                                $json .= '{ "match": { "'.$singleFieldArray[$x][0].'" : "'.$finalQuery.'" }}';
                            }else{
                                $json .= '{ "match": { "'.$singleFieldArray[$x][0].'" : "'.$finalQuery.'" }},';
                            }
                            
                        }else{
                            if(count($manyFieldArray)-1 == $x){
                                $json .= '{ "match": { "'.$singleFieldArray[$x][0].'" : "'.$singleFieldArray[$x][1].'" }}';
                            }else{
                                $json .= '{ "match": { "'.$singleFieldArray[$x][0].'" : "'.$singleFieldArray[$x][1].'" }},';
                            }
                            
                        }
                    }
                    
                    $json .= ']}}}';
                }else{
                    $singleFieldArray = explode(' ',$searchValue);
                    $newArr = [];
                    for ($x = 0; $x < count($singleFieldArray); $x++) {
                        if($x != 0){
                            array_push($newArr , $singleFieldArray[$x]);
                        }
                    }
                    $finalQuery = implode(' ', $newArr);
                    $json = '
                    {
                        "query": {
                          "bool": {
                            "must": [
                              {
                                "match_phrase": {
                                  "'.$singleFieldArray[0].'":  "'.$finalQuery.'"
                                }
                              },
                              {
                                "match_phrase": {
                                  "table_name":  "'.$request->input('domain').'"
                                }
                              }
                            ]
                          }
                        }
                      }
                      '; 
                }
            }else{
                $json = '{
                    "query" : {
                        "query_string" : {
                            "query" : "'.$searchValue.'"
                        }
                    }
                }';
            }
            // var_dump($json);
            $client = ClientBuilder::create()->build();
            $params = [
                "size" => $sizevalue, 
                'from'=> $startValue,
                'index' => 'penduduk',
                'type' => 'doc',
                'body' => $json
            ];
            $response = $client->search($params);
            if($response){
                $hits = $response['hits']['total'];
            }
        }
        return view('welcome')->with([
            'response'=> $response, 
            'searchValue'=>$searchValue, 
            'startValue'=>$start,
            'totalPages'=>$hits
            ]);
    }
}